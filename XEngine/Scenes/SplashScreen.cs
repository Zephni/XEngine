﻿using Microsoft.Xna.Framework;

namespace XEngine.Scenes
{
    class SplashScreen : Scene
    {
        public override void Start()
        {
            this.BackgroundColor = Color.White;

            new GameObject(obj => {
                DrawableRectangle dr = obj.AddComponent<DrawableRectangle>();
                dr.Size = Global.VirtualScreen.Size;
                obj.BatchLayerIndex = 1;

                CoroutineHelper.WaitRun(2, () => {
                    UpdateAction(() => {
                        dr.Opacity -= Global.DeltaTime / 2;
                        return dr.Opacity > 0;
                    });
                });
            });

            new GameObject(obj => {
                obj.AddComponent<Sprite>().LoadTexture("MugHeadLogo");
                obj.transform.Scale = new Vector2(0.4f, 0.4f);
                obj.transform.Origin = new Vector2(0.5f, 0.5f);
                obj.transform.Position = Global.MainCamera.Position;
            });
        }
    }
}

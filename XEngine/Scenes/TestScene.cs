﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace XEngine.Scenes
{
    class TestScene : Scene
    {
        public override void Start()
        {
            List<BoxCollider> FloorColliders = new List<BoxCollider>();

            string[] XYTiles = new string[8];
            XYTiles[0] = "11111111";
            XYTiles[1] = "1      1";
            XYTiles[2] = "1      1";
            XYTiles[3] = "1      1";
            XYTiles[4] = "1      1";
            XYTiles[5] = "1      1";
            XYTiles[6] = "1     11";
            XYTiles[7] = "11111111";

            for (int y = 0; y < 8; y++)
            {
                char[] chararray = XYTiles[y].ToCharArray();

                for (int x = 0; x < 8; x++)
                {
                    if (chararray[x] == '1')
                        new GameObject(go => {
                            go.AddComponent<DrawableRectangle>();
                            go.transform.Position = new Vector2(x * 32, y * 32);
                            FloorColliders.Add(go.AddComponent<BoxCollider>());
                        });
                }
            }

            new GameObject(player => {
                int CurrentJump = 0;
                int MaxJumps = 2;
                float Gravity = 0.2f;
                float Acceleration = 0.5f;
                float Deceleration = 0.1f;
                float XMove = 0;
                float YMove = 0;
                float MaxXMove = 2;
                float JumpStrength = 4;
                bool Grounded = false;

                Sprite sprite = player.AddComponent<Sprite>();
                sprite.LoadTexture("test");

                BoxCollider collider = player.AddComponent<BoxCollider>();

                player.transform.Position = new Vector2(100, 100);

                Global.UpdateActions.Add(() => {
                    // Defaults
                    Grounded = false;

                    // Limits
                    XMove = (XMove > MaxXMove) ? MaxXMove : (XMove < -MaxXMove) ? -MaxXMove : XMove;
                    YMove = (YMove > JumpStrength) ? JumpStrength : (YMove < -JumpStrength) ? -JumpStrength : YMove;

                    // X, Y Loops
                    for(int X = 0; X < System.Math.Abs(XMove); X++)
                    {
                        player.transform.Position += new Vector2((XMove > 0) ? 1 : -1, 0);

                        // Collision
                        if(collider.CollidingWith(FloorColliders))
                        {
                            player.transform.Position += new Vector2((XMove > 0) ? -1 : 1, 0);
                            XMove = 0;
                            break;
                        }
                    }

                    for(int Y = 0; Y < System.Math.Abs(YMove); Y++)
                    {
                        player.transform.Position += new Vector2(0, (YMove > 0) ? 1 : -1);

                        // Collision
                        if(collider.CollidingWith(FloorColliders))
                        {
                            player.transform.Position += new Vector2(0, -1);
                            YMove = 0;
                            CurrentJump = 0;
                            Grounded = true;
                            break;
                        }
                    }

                    // Gravity
                    YMove += Gravity;

                    // Deceleration
                    XMove += (XMove > 0) ? -Deceleration : (XMove < 0) ? Deceleration : 0;
                    XMove = (XMove > -0.1f && XMove < 0.1f) ? 0 : XMove;

                    // X Movement
                    if (Keyboard.GetState().IsKeyDown(Keys.Right))
                        XMove += Acceleration;
                    if (Keyboard.GetState().IsKeyDown(Keys.Left))
                        XMove += -Acceleration;

                    // Y Movement
                    if(Grounded && CurrentJump < MaxJumps && Keyboard.GetState().IsKeyDown(Keys.Z))
                    {
                        CurrentJump++;
                        YMove = -JumpStrength;
                    }

                    // Camera
                    float CamXMove = System.Math.Abs(player.transform.Position.X - Global.MainCamera.Position.X) / 4;
                    float CamYMove = System.Math.Abs(player.transform.Position.Y - Global.MainCamera.Position.Y) / 4;
                    Global.MainCamera.Position.X += (player.transform.Position.X > Global.MainCamera.Position.X) ? CamXMove : (player.transform.Position.X < Global.MainCamera.Position.X) ? -CamXMove : 0;
                    Global.MainCamera.Position.Y += (player.transform.Position.Y > Global.MainCamera.Position.Y) ? CamYMove : (player.transform.Position.Y < Global.MainCamera.Position.Y) ? -CamYMove : 0;

                    System.Diagnostics.Debug.WriteLine(player.transform.Position + ", " + Global.MainCamera.Position);

                    return true;
                });
            });
        }
    }
}
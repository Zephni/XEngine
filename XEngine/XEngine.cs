﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace XEngine
{
    public class XEngine : Game
    {
        public XEngine()
        {
            Global.Game = this;
            Global.Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // BackBuffer size
            Global.Graphics.PreferredBackBufferWidth = 512;
            Global.Graphics.PreferredBackBufferHeight = 480;
        }

        protected override void Initialize()
        {
            // Build layers
            Global.BatchLayers = new Dictionary<int, BatchLayer>();
            Global.BatchLayers.Add(0, new BatchLayer(layer => {
                layer.CFG_Matrix = () => { return Global.MainCamera.GetTransformation(); };
            }));
            Global.BatchLayers.Add(1, new BatchLayer());

            // Virtual screen
            Global.VirtualScreen = new VirtualScreen(new Point(512 / 2, 480 / 2));

            // Camera
            Global.MainCamera = new Camera(Global.VirtualScreen);

            // Scene manager
            Global.SceneManager = new SceneManager();

            // Global method actions
            Global.UpdateActions = new List<BooleanAction>();

            // FrameRate
            Global.FrameRate = new FrameRate();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // First scene to load
            Global.SceneManager.LoadScene(new Scenes.SplashScreen());
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            Global.DeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Global.FrameRate.Update(Global.DeltaTime);

            Global.SceneManager.CurrentScene.Update();
            Global.VirtualScreen.Update();

            for (int I = 0; I < Global.UpdateActions.Count; I++)
                if(!Global.UpdateActions[I]())
                    Global.UpdateActions.Remove(Global.UpdateActions[I]);

            Coroutines.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            Global.VirtualScreen.Draw();
            base.Draw(gameTime);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace XEngine
{
    public class Anim
    {
        public Point[] Frames;
        public float Speed;
        public bool Loop;

        public Anim(Point[] frames, float speed = 0.5f, bool loop = true)
        {
            this.Frames = frames;
            this.Speed = speed;
            this.Loop = loop;
        }
    }
}

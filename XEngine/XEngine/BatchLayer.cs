﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace XEngine
{
    public class BatchLayer
    {
        public SpriteBatch SpriteBatch;
        public List<GameObject> Objects;

        private float opacity = 1;
        public float Opacity
        {
            get { return this.opacity; }
            set { this.opacity = Global.WithinRange(0, 1, value);}
        }

        public SpriteSortMode CFG_SpriteSortMode = SpriteSortMode.Deferred;
        public BlendState CFG_BlendState = null;
        public SamplerState CFG_SampleState = SamplerState.PointClamp;
        public DepthStencilState CFG_DepthStencilState = null;
        public RasterizerState CFG_RasterizerState = null;
        public Effect CFG_Effect = null;
        public MatrixAction CFG_Matrix = () => { return Matrix.Identity; };

        public BatchLayer(SingleParamAction<BatchLayer> Setup = null)
        {
            this.SpriteBatch = new SpriteBatch(Global.Graphics.GraphicsDevice);
            this.Objects = new List<GameObject>();

            if(Setup != null)
                Setup(this);
        }

        public void Update()
        {
            foreach (GameObject obj in this.Objects)
                obj.Update();
        }

        public virtual void Draw()
        {
            this.SpriteBatch.Begin(
                this.CFG_SpriteSortMode,
                this.CFG_BlendState,
                this.CFG_SampleState,
                this.CFG_DepthStencilState,
                this.CFG_RasterizerState,
                this.CFG_Effect,
                this.CFG_Matrix()
            );

            foreach (GameObject obj in this.Objects)
                obj.Draw(this.SpriteBatch);

            this.SpriteBatch.End();
        }
    }
}

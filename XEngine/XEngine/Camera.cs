﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XEngine
{
    public class Camera
    {
        public Vector2 Position;

        public float Rotation;

        public float zoom;
        public float Zoom
        {
            get { return this.zoom; }
            set { this.zoom = value; if (this.zoom < 0.0f) this.zoom = 0; }
        }

        public Matrix Transform;

        private VirtualScreen virtualScreen;

        public Camera(VirtualScreen virtualScreen)
        {
            this.virtualScreen = virtualScreen;
            this.Zoom = 1f;
            this.Rotation = 0f;
            this.Position = new Vector2(virtualScreen.VirtualWidth / 2, virtualScreen.VirtualHeight / 2);
        }

        public void Move(Vector2 Amount)
        {
            this.Position += Amount;
        }

        public Matrix GetTransformation()
        {
            this.Transform =
              Matrix.CreateTranslation(new Vector3(-this.Position.X, -this.Position.Y, 0)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(virtualScreen.VirtualWidth * 0.5f, virtualScreen.VirtualHeight * 0.5f, 0));
            return this.Transform;
        }
    }
}

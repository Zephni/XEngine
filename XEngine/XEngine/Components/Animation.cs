﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace XEngine
{
    public class Animation : Sprite
    {
        public bool Paused = false;
        public int AnimFrame = 0;
        
        private Dictionary<string, Anim> AnimList = new Dictionary<string, Anim>();

        private Anim currentAnim = null;
        public Anim CurrentAnim
        {
            get { return currentAnim; }
            private set { this.currentAnim = value; }
        }

        public void Add(string alias, Anim animation)
        {
            this.AnimList.Add(alias, animation);
        }

        public void Run(string alias, float? speed = null, bool? loop = null)
        {
            this.Run(this.AnimList[alias], speed, loop);
        }

        public void Run(Anim animation, float? speed = null, bool? loop = null)
        {
            this.CurrentAnim = animation;

            if (speed != null)
                this.CurrentAnim.Speed = (float)speed;

            if (loop != null)
                this.CurrentAnim.Loop = (bool)loop;

            CoroutineHelper.RunOverX(this.CurrentAnim.Speed, this.CurrentAnim.Frames.Length, frame => {
                this.AnimFrame = frame;
                this.SetFrame(this.CurrentAnim.Frames[this.AnimFrame].X, this.CurrentAnim.Frames[this.AnimFrame].Y);
            }, () => {
                if(this.CurrentAnim.Loop)
                    this.Run(animation, speed, loop);
            });
        }

        public void Stop()
        {
            this.CurrentAnim = null;
        }
    }
}

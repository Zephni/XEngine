﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace XEngine
{
    public class Drawable : XBehaviour
    {
        public float SortingLayer = 0;
        public int SpriteBatchLayer = 0;

        public Color color = Color.White;
        public virtual Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        private Vector2 size = Vector2.Zero;
        public virtual Vector2 Size
        {
            get { return this.size; }
            set { this.size = value; }
        }

        public float Opacity = 1;
        public SpriteEffects SpriteEffect = SpriteEffects.None;

        public Texture2D Texture;

        public Texture2D LoadTexture(string FileName)
        {
            this.Texture = Global.Game.Content.Load<Texture2D>(FileName);
            this.Size = new Vector2(this.Texture.Width, this.Texture.Height);
            return this.Texture;
        }

        public Vector2 ActualSize
        {
            get { return this.Size * this.gameObject.transform.Scale; }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                this.Texture,
                this.gameObject.transform.Position,
                null,
                this.Color * this.Opacity,
                0,
                new Vector2(gameObject.transform.Origin.X * this.Size.X, gameObject.transform.Origin.Y * this.Size.Y),
                this.gameObject.transform.Scale,
                SpriteEffects.None,
                0
            );
        }
    }
}

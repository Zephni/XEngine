﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace XEngine
{
    class DrawableRectangle : Drawable
    {
        public override Color Color
        {
            get { return base.Color; }
            set
            {
                base.Color = value;
                this.BuildTexture();
            }
        }

        public override Vector2 Size
        {
            get { return base.Size; }
            set
            {
                base.Size = value;
                this.BuildTexture();
            }
        }

        public override void Start()
        {
            base.Start();
            this.Size = new Vector2(32, 32);
            this.Color = Color.Black;
            this.BuildTexture();
        }

        private void BuildTexture()
        {
            Color color = this.Color;
            int x = (int)this.Size.X;
            int y = (int)this.Size.Y;

            this.Texture = new Texture2D(Global.Graphics.GraphicsDevice, x, y);

            Color[] data = new Color[x * y];

            for (int i = 0; i < data.Length; ++i) data[i] = color;
                this.Texture.SetData(data);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace XEngine
{
    public class Sprite : Drawable
    {
        public Texture2D[,] Frames = new Texture2D[0, 0];

        private Point currentFrame = Point.Zero;
        public Point CurrentFrame
        {
            get { return currentFrame; }
            set
            {
                if (value.X >= this.Frames.GetLength(0)) value.X = 0;
                if (value.X < 0) value.X = this.Frames.GetLength(0) - 1;
                if (value.Y >= this.Frames.GetLength(1)) value.Y = 0;
                if (value.Y < 0) value.Y = this.Frames.GetLength(1) - 1;

                this.currentFrame = value;
            }
        }

        public void SetFrame(int X, int Y)
        {
            this.CurrentFrame = new Point(X, Y);
            this.Texture = this.Frames[this.CurrentFrame.X, this.CurrentFrame.Y];
            this.Size = new Vector2(this.Texture.Width, this.Texture.Height);
        }

        public void SetFrames(int TotalX, int TotalY)
        {
            this.Frames = new Texture2D[TotalX, TotalY];

            Point FrameSize = new Point(this.Texture.Width / TotalX, this.Texture.Height / TotalY);

            for (int X = 0; X < this.Frames.GetLength(0); X++)
            {
                for (int Y = 0; Y < this.Frames.GetLength(1); Y++)
                {
                    Rectangle sourceRectangle = new Rectangle(new Point(X * FrameSize.X, Y * FrameSize.Y), FrameSize);

                    this.Frames[X, Y] = new Texture2D(Global.Game.GraphicsDevice, sourceRectangle.Width, sourceRectangle.Height);
                    Color[] data = new Color[sourceRectangle.Width * sourceRectangle.Height];
                    this.Texture.GetData(0, sourceRectangle, data, 0, data.Length);
                    this.Frames[X, Y].SetData(data);
                }
            }

            this.SetFrame(0, 0);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XEngine
{
    ///
    /// To add a new font, open Content.mgcb and add new item "Font Description", then build. Then grab the .xnb from
    /// the /bin folder and drag it into the Content folder within VS.
    ///

    /// <summary>
    /// Text component, set the SpriteFont to a .xnb loaded content. And set the text.
    /// </summary>
    class Text : XBehaviour
    {
        public string text = "";
        public Color Color = Color.Black;
        public float Opacity = 1;
        public SpriteFont SpriteFont = null;

        public Text SetSpriteFont(string FontAlias = "Arial-12")
        {
            this.SpriteFont = Global.Game.Content.Load<SpriteFont>("Fonts/"+FontAlias);
            return this;
        }

        public override void Start()
        {
            this.SetSpriteFont();
        }
    }
}

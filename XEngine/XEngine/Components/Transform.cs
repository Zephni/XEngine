﻿using Microsoft.Xna.Framework;
using System;


namespace XEngine
{
    public class Transform : XBehaviour
    {
        public Vector2 LocalPosition = Vector2.Zero;

        private Vector2 position = Vector2.Zero;
        public Vector2 Position
        {
            get { return this.LocalToWorldPosition(); }
            set { this.position = value; }
        }

        public Vector2 LocalToWorldPosition()
        {
            if(this.gameObject.Parent != null)
                return this.gameObject.Parent.transform.LocalToWorldPosition() + LocalPosition;
            else
                return this.position;
        }

        private Vector2 origin = Vector2.Zero;
        public Vector2 Origin
        {
            get { return this.origin; }
            set { this.origin = new Vector2(MathHelper.Clamp(value.X, 0f, 1f), MathHelper.Clamp(value.Y, 0f, 1f)); }
        }

        public float Rotation = 0;

        private Vector2 scale = new Vector2(1, 1);
        public Vector2 Scale
        {
            get { return this.scale; }
            set {
                value.X = MathHelper.Clamp(value.X, 0, 1);
                value.Y = MathHelper.Clamp(value.Y, 0, 1);
                this.scale = value;
            }
        }

        public float ScaleXY
        {
            get { return this.Scale.X; }
            set { this.Scale = new Vector2(value, value); }
        }
    }
}

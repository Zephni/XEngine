﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace XEngine
{
    public class GameObject
    {
        public List<object> Components;
        public Transform transform;
        public int BatchLayerIndex = 0;
        public GameObject Parent;

        public GameObject(SingleParamAction<GameObject> mhsAction)
        {
            this.Components = new List<object>();
            this.transform = this.AddComponent<Transform>();
            mhsAction(this);
            Global.BatchLayers[this.BatchLayerIndex].Objects.Add(this);
        }

        public T AddComponent<T>(SingleParamAction<T> xBehaviourAction = null)
        {
            T component = (T)Activator.CreateInstance(typeof(T));

            Type thisType = component.GetType();
            FieldInfo go = thisType.GetField("gameObject");
            go.SetValue(component, this);

            this.Components.Add(component);

            if (xBehaviourAction != null)
                xBehaviourAction(component);

            return component;
        }

        public T GetComponent<T>(SingleParamAction<T> xBehaviourAction = null)
        {
            T Obj = this.Components.OfType<T>().First();

            if (xBehaviourAction != null)
                xBehaviourAction((T)Obj);

            return Obj;
        }

        public bool HasComponent<T>()
        {
            return this.Components.OfType<T>().Count() > 0;
        }

        public void Update()
        {
            foreach(XBehaviour component in this.Components)
                component.Update();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (XBehaviour component in this.Components)
                component.Draw(spriteBatch);
        }
    }
}
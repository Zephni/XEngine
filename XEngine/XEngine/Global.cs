﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace XEngine
{
    public static class Global
    {
        public static Game Game;
        public static SceneManager SceneManager;
        public static GraphicsDeviceManager Graphics;
        public static VirtualScreen VirtualScreen;
        public static Camera MainCamera;
        public static Dictionary<int, BatchLayer> BatchLayers;
        public static List<BooleanAction> UpdateActions;
        public static float DeltaTime;
        public static FrameRate FrameRate;

        public static float WithinRange(float Min, float Max, float Value)
        {
            if(Value < Min) return Min;
            else if(Value > Max) return Max;
            else return Value;
        }
    }
}

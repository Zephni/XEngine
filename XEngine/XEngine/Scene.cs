﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace XEngine
{
    public class Scene
    {
        public Color BackgroundColor;

        public Scene()
        {
            this.BackgroundColor = Color.White;
            this.Start();
        }

        public void UpdateAction(BooleanAction Param)
        {
            Global.UpdateActions.Add(Param);
        }

        public virtual void Start()
        {

        }

        public virtual void End()
        {

        }

        /// <summary>
        /// The current scene will update every game update
        /// </summary>
        public void Update()
        {
            foreach (KeyValuePair<int, BatchLayer> layer in Global.BatchLayers)
                layer.Value.Update();
        }

        /// <summary>
        /// The current scene will draw every game draw update
        /// </summary>
        public void Draw()
        {
            Global.Graphics.GraphicsDevice.Clear(this.BackgroundColor);

            foreach(KeyValuePair<int, BatchLayer> layer in Global.BatchLayers)
                layer.Value.Draw();
        }
    }
}
﻿using System.Collections.Generic;

namespace XEngine
{
    public class SceneManager
    {
        public Scene CurrentScene = null;

        /// <summary>
        /// Ends any current scene, destroys all non transversable GameObjects and starts the passed scene
        /// </summary>
        /// <param name="scene"></param>
        public void LoadScene(Scene scene)
        {
            if(this.CurrentScene != null)
                this.CurrentScene.End();

            foreach (KeyValuePair<int, BatchLayer> layer in Global.BatchLayers)
                layer.Value.Objects = new List<GameObject>();

            this.CurrentScene = scene;
            this.CurrentScene.Start();
        }
    }
}
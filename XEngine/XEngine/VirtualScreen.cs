﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XEngine
{
    public class VirtualScreen
    {
        public Vector2 Size
        {
            get { return new Vector2(VirtualWidth, VirtualHeight); }
        }

        public readonly int VirtualWidth;
        public readonly int VirtualHeight;
        public readonly float VirtualAspectRatio;
        
        private SpriteBatch spriteBatch;
        private GraphicsDevice graphicsDevice;
        private RenderTarget2D screen;

        public VirtualScreen(Point Resolution)
        {
            VirtualWidth = Resolution.X;
            VirtualHeight = Resolution.Y;
            VirtualAspectRatio = (float)(VirtualWidth) / (float)(VirtualHeight);

            this.graphicsDevice = Global.Graphics.GraphicsDevice;
            this.spriteBatch = new SpriteBatch(this.graphicsDevice);
            screen = new RenderTarget2D(graphicsDevice, VirtualWidth, VirtualHeight, false, graphicsDevice.PresentationParameters.BackBufferFormat, graphicsDevice.PresentationParameters.DepthStencilFormat, graphicsDevice.PresentationParameters.MultiSampleCount, RenderTargetUsage.DiscardContents);
        }

        private bool areaIsDirty = true;

        public void PhysicalResolutionChanged()
        {
            areaIsDirty = true;
        }

        private Rectangle area;

        public void Update()
        {
            if (!areaIsDirty)
            {
                return;
            }

            areaIsDirty = false;
            var physicalWidth = graphicsDevice.Viewport.Width;
            var physicalHeight = graphicsDevice.Viewport.Height;
            var physicalAspectRatio = graphicsDevice.Viewport.AspectRatio;

            if ((int)(physicalAspectRatio * 10) == (int)(VirtualAspectRatio * 10))
            {
                area = new Rectangle(0, 0, physicalWidth, physicalHeight);
                return;
            }

            if (VirtualAspectRatio > physicalAspectRatio)
            {
                var scaling = (float)physicalWidth / (float)VirtualWidth;
                var width = (float)(VirtualWidth) * scaling;
                var height = (float)(VirtualHeight) * scaling;
                var borderSize = (int)((physicalHeight - height) / 2);
                area = new Rectangle(0, borderSize, (int)width, (int)height);
            }
            else
            {
                var scaling = (float)physicalHeight / (float)VirtualHeight;
                var width = (float)(VirtualWidth) * scaling;
                var height = (float)(VirtualHeight) * scaling;
                var borderSize = (int)((physicalWidth - width) / 2);
                area = new Rectangle(borderSize, 0, (int)width, (int)height);
            }
        }

        public void BeginCapture()
        {
            graphicsDevice.SetRenderTarget(screen);
        }

        public void EndCapture()
        {
            graphicsDevice.SetRenderTarget(null);
        }

        public void Draw()
        {
            this.BeginCapture();
            Global.SceneManager.CurrentScene.Draw();
            this.EndCapture();

            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, null);
            spriteBatch.Draw(screen, area, Color.White);
            spriteBatch.End();
        }
    }
}

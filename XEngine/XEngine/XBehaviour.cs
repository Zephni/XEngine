﻿using Microsoft.Xna.Framework.Graphics;

namespace XEngine
{
    /// <summary>
    /// Inheritable component for attaching to GameObjects
    /// </summary>
    public class XBehaviour
    {
        public GameObject gameObject;

        public XBehaviour()
        {
            this.Start();
        }

        virtual public void Start()
        {

        }

        virtual public void Update()
        {

        }

        virtual public void Draw(SpriteBatch spriteBatch)
        {

        }
    }
}
